package com.example.andreaboetto.adaptertest;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MyActivity extends Activity {
    PersonsAdapter personAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("utente 1","test@top-ix.org",R.drawable.mario));
        persons.add(new Person("utente2","test2@top-ix.org",R.drawable.football));
        setContentView(R.layout.activity_my);
        ListView personList= (ListView) findViewById(R.id.persons_list);
        View header =getLayoutInflater().inflate(R.layout.person_list_item_layout,null);
        ((TextView)header.findViewById(R.id.person_list_item_name)).setText(getResources().getString(R.string.name));
        ((TextView)header.findViewById(R.id.person_list_item_surname)).setText(getResources().getString(R.string.surname));
        personList.addHeaderView(header);
        personAdapter = new PersonsAdapter(this, R.layout.person_list_item_layout);
        personAdapter.addAll(persons);
        personList.setAdapter(personAdapter);
        Log.d("oncreate","created");
    }

    @Override
    protected void onResume() {
        super.onResume();
        AsyncHttpClient client=new AsyncHttpClient();
        client.get("http://194.116.72.190/member/2/?json",new MyCustomJsonHandler());
        client.get("http://194.116.72.190/member/3/?json",new MyCustomJsonHandler());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private class MyCustomJsonHandler extends JsonHttpResponseHandler {
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            Log.d("handler","successJsonobj");
            Person p=new Person(response.optString("name",""),response.optString("email",""),R.drawable.mario);
            personAdapter.add(p);
            personAdapter.notifyDataSetChanged();
        }
        @Override
        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
            Log.d("handler","statuscode "+statusCode);
            super.onFailure(statusCode, headers, throwable, errorResponse);
        }


    }


}


