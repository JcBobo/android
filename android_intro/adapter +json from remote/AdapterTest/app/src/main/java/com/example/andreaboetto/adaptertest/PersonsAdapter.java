package com.example.andreaboetto.adaptertest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Andrea Boetto on 04/07/2014.
 */
public class PersonsAdapter extends ArrayAdapter<Person> {
    private final int layout;

    public PersonsAdapter(Context context, int resource) {
        super(context, resource);
        layout=resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView==null)
            convertView = LayoutInflater.from(getContext()).inflate(layout, null);

        TextView name=(TextView)convertView.findViewById(R.id.person_list_item_name);
        TextView surname=(TextView)convertView.findViewById(R.id.person_list_item_surname);
        ImageView img=(ImageView)convertView.findViewById(R.id.person_list_item_img);
        Person p =getItem(position);
        name.setText(p.getName().toString());
        surname.setText(p.getSurname().toString());
        img.setImageDrawable(getContext().getResources().getDrawable(p.getImg()));
        return convertView;
    }
}

